export default class ClsMedia {

    private tmpSegundaNota: number = 0
    private tmpPrimeiraNota: number = 0

    protected media: number = 0

    public get primeiraNota(): number {
        return this.tmpPrimeiraNota
    }

    public set primeiraNota(nota: number) {
        this.tmpPrimeiraNota = nota
        this.calcularMedia()
    }

    public get segundaNota(): number {
        console.log('Estou dentro do GET da Segunda Nota....')
        return this.tmpSegundaNota
    }

    public set segundaNota(nota: number) {
        console.log('Estou dentro do SET da Segunda Nota.... Parametro Recebido: ', nota)
        this.tmpSegundaNota = nota
        this.calcularMedia()
    }

    private calcularMedia(): void {
        this.media = (this.primeiraNota + this.segundaNota) / 2
    }

}