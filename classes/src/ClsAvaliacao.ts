import ClsMedia from "./ClsMedia"

export const MEDIA_APROVACAO: number = 7

export const MEDIA_RECUPERACAO: number = 5

export default class ClsAvaliacao extends ClsMedia {

    public get resultadoAvaliacao(): string {
        
        if (this.media >= MEDIA_APROVACAO) {
            return 'Aprovado'
        } else if (this.media >= MEDIA_RECUPERACAO) {
            return 'Recuperação'
        } else {
            return 'Reprovado'
        }

    }

}