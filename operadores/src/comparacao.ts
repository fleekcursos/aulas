let x: number = 10

let y: number = 3

console.log('x == y', x == y)

//@ts-ignore
console.log("'3' == 3", '3' == 3)

//@ts-ignore
console.log("'3' == y", '3' == y)

//@ts-ignore
console.log("'3' == x", '3' == x)

//@ts-ignore
console.log("'3' === 3", '3' === 3)

//@ts-ignore
console.log("'3' === y", '3' === y)

//@ts-ignore
console.log("'3' === x", '3' === x)
