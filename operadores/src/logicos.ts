/*

e --> && --> and

ou --> || --> or

não --> ! --> not

1 - uma maça tem cor avermelhada ou azulada 
==> True (Verdadeiro)

2 - uma maça tem cor avermelhada e azulada
==> False (Falso)

3 - uma maça tem cor avermelhada e não azulada
==> True ()

*/


let c: boolean = true

let d: boolean = false

console.log(c && d)

console.log(c || d)

console.log(c && !d)

console.log(!c && !d)





let e: number = 10

let f: number = 15

let g: number = 12

console.log("Agora com Números....")
console.log(e > f && g < f)

console.log(e > f || g < f)

// 10 > 15 && 12 < 15
