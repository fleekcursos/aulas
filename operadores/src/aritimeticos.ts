let a: number = 1

console.log('Valor de a: ', a)

console.log('Valor de ++a:', ++a)

console.log('Valor de a++:', a++)

console.log('Valor de a final: ', a)

console.log('\n\n\n\nOrdem dos operadores')
console.log('====================')

console.log(2 + 3 - 5 ** 2 * 2 / 2)

console.log('\n\n\nAlterando a ordem')
console.log('====================')

console.log((2 + 3 - 5) ** 2 * 2 / 2)

console.log('\n\n\nSimulando Erro')
console.log('====================')
console.log(2 / 0)
