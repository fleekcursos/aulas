// Exemplo 01
console.log('\n\n\nExemplo 01')
console.log('==========')

for (let x: number = 0; x < 10; x = x + 2) {
    console.log(x)
}

// Exemplo 02
console.log('\n\n\nExemplo 02 - in')
console.log('===============')

let nomes: Array<String> = ["Fleek", "Cursos", "Zanatta"]

for (let x in nomes) {
    console.log(x)
}

// Exemplo 03
console.log('\n\n\nExemplo 03 - of')
console.log('===============')

for (let x of nomes) {
    console.log(x)
}

// Exemplo 04
console.log('\n\n\nExemplo 04 - while')
console.log('===============')

let contador: number = 0

while (++contador < 10) {
    console.log('Valor de Contador: ', contador)
}

// Exemplo 05
console.log('\n\n\nExemplo 05 - do')
console.log('===============')

let contadorDo: number = 0

do {
    console.log('Valor de Contador: ', contadorDo)
} while (contadorDo++ < 10)

