interface colaboradorInterface {
    nome: string
    empresa: string
    idColaborador: number
}

let rsColaborador: colaboradorInterface

rsColaborador = {
    idColaborador: 100,
    nome: "Zanatta",
    empresa: "Fleek Cursos"
}

// console.log(typeof rsColaborador)

// console.log(rsColaborador)

let c = JSON.stringify(rsColaborador)

console.log('\n\n\n\n')

console.log('JSON:')
console.log('======')

console.log(typeof c)

console.log(c)

console.log('\n\n\n\n')

console.log('OBJETO JAVASCRIPT:')
console.log('==================')

let d = JSON.parse(c)

console.log(typeof d)

console.log(d)

console.log('\n\n\n\n')


