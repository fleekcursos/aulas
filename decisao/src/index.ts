const MEDIA_APROVACAO: number = 7

const MEDIA_RECUPERACAO: number = 5

const NOTA_ALUNO: number = 5

if (NOTA_ALUNO >= MEDIA_APROVACAO) {

    console.log('Aprovado (teste if)')

} else if (NOTA_ALUNO >= MEDIA_RECUPERACAO) {

    console.log('Aluno em Recuperação. (teste if)')

} else {

    console.log('Aluno Reprovado. (teste if)')

}

let resultado: string = NOTA_ALUNO >= MEDIA_APROVACAO ? 'Aprovado' :
    NOTA_ALUNO >= MEDIA_RECUPERACAO ? 'Recuperação' : 'Reprovado'

console.log('Por Operador Ternário...')
console.log(resultado)

switch (resultado) {
    case 'Aprovado':
        console.log('Parabéns!')
        break

    case 'Recuperação':
        console.log('Não Desista')
        break

    default:
        console.log('Melhore os Estudos!!!')
        break
}



