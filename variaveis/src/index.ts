
/**
 * Imprime em tela o conteúdo das variáveis
 * @param {string} descritivo Qual é o descritivo que será impresso
 * @param {number | string | boolean} oque Conteúdo que será impresso
 */
function imprimir(descritivo: string, oque: number | string | boolean): void {
    console.log(descritivo, oque)
}

/**
 * Soma dois números e retorna o resultado
 * @param {number} numero Primeiro Número da Soma
 * @param {number} soma Valor a ser acrescentado
 */
function somar(numero: number, soma: number): number {
    let x: number

    numero = 50

    x = numero + soma

    return x
}
/**
 * Recebe Objeto com x e y e retorna a soma
 * @param objeto Objeto com x e y
 * @returns Soma de x + y
 */
function somarObjeto(objeto: { x: number, y: number }): number {

    objeto.x = 100
    objeto.y = 200

    return objeto.x + objeto.y
}

var x: number = 10
var y: number = 20

var numeros: { x: number, y: number } = {
    x: 10,
    y: 20
}

var somatoria: number = somar(x, y)

var teste: number = somar(numeros.x, numeros.y)

imprimir("Soma do Objeto: ", somarObjeto(numeros))

imprimir("Soma", somatoria)

imprimir("\n\nValor da variável x: ", x)
imprimir("\n\nValor da variável y: ", y)

imprimir("\n\nValor do objeto x: ", numeros.x)
imprimir("\n\nValor do objeto y: ", numeros.y)
